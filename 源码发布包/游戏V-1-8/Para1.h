void CMainFrame::Para1Init(void)
{
/////////////////////////////////////////////////////////////////////////////
//调入所需对话，主角图片，地图文件群。
	LoadDialog("dlg\\para1.dlg");
	LoadActor("pic\\hero.bmp");//调入主角
/////////////////////////////////////////////////////////////////////////////
//调入所需头像
	hbmp_Photo0=(HBITMAP)LoadImage(NULL,"pic\\pic300.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_Photo1=(HBITMAP)LoadImage(NULL,"pic\\pic301.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫隆
	hbmp_Photo2=(HBITMAP)LoadImage(NULL,"pic\\pic302.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子
	hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic303.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//白莲教教徒
	hbmp_Photo4=(HBITMAP)LoadImage(NULL,"pic\\pic304.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卧虫
	hbmp_Photo5=(HBITMAP)LoadImage(NULL,"pic\\pic310.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（1）
	hbmp_Photo6=(HBITMAP)LoadImage(NULL,"pic\\pic311.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（2）
/////////////////////////////////////////////////////////////////////////////
//调入所需道具
	hbmp_Prop0=(HBITMAP)LoadImage(NULL,"pic\\pic200.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//轩辕剑
	hbmp_Prop1=(HBITMAP)LoadImage(NULL,"pic\\pic201.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//食物
/////////////////////////////////////////////////////////////////////////////
//调入其他图片
	hbmp_0=(HBITMAP)LoadImage(NULL,"pic\\pic100.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_1=(HBITMAP)LoadImage(NULL,"pic\\pic101.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_2=(HBITMAP)LoadImage(NULL,"pic\\pic102.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_3=(HBITMAP)LoadImage(NULL,"pic\\pic103.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_4=(HBITMAP)LoadImage(NULL,"pic\\pic104.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_5=(HBITMAP)LoadImage(NULL,"pic\\pic105.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_6=(HBITMAP)LoadImage(NULL,"pic\\pic001.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_7=(HBITMAP)LoadImage(NULL,"pic\\pic002.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_8=(HBITMAP)LoadImage(NULL,"pic\\pic003.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_9=(HBITMAP)LoadImage(NULL,"pic\\pic004.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_10=(HBITMAP)LoadImage(NULL,"pic\\pic005.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_11=(HBITMAP)LoadImage(NULL,"pic\\pic006.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_12=(HBITMAP)LoadImage(NULL,"pic\\pic007.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_13=(HBITMAP)LoadImage(NULL,"pic\\pic008.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_14=(HBITMAP)LoadImage(NULL,"pic\\pic009.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_15=(HBITMAP)LoadImage(NULL,"pic\\pic010.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_16=(HBITMAP)LoadImage(NULL,"pic\\pic011.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_17=(HBITMAP)LoadImage(NULL,"pic\\pic012.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_18=(HBITMAP)LoadImage(NULL,"pic\\pic013.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_19=(HBITMAP)LoadImage(NULL,"pic\\pic014.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_20=(HBITMAP)LoadImage(NULL,"pic\\pic015.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	hbmp_21=(HBITMAP)LoadImage(NULL,"pic\\pic016.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
/////////////////////////////////////////////////////////////////////////////
//引入开始游戏的位置
	if(st_sub1==0){
		m_info_st=9;//对话态
		m_oldinfo_st=9;
	}
/////////////////////////////////////////////////////////////////////////////
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para1Info(int type)
{
	int i;

if(st_sub1==0){
	if(st_dialog==0)
	{
		PlayMidi("midi\\p1004.mid");
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_6);
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,SRCCOPY);
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		//以下的语句用于解决显示一个next按钮的问题
		m_oldinfo_map=0;
		m_oldinfo_next=1;
		m_info_next=1;//设定为NEXT模式
		m_info_map=0;//不允许点击地图
		st_dialog=1;//设置开始位置
		pdc->StretchBlt(31,529,58,17,&InfoDC,797,87,1,19,SRCAND);
		RenewInfo(pdc);
		ReleaseDC(pdc);
		pdc = NULL;
	}
	else if(st_dialog==1)
	{
		MemDC.SelectObject(hbmp_7);//轩辕剑造型
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,SRCCOPY);
		vopen();
		int k=0;
		CDC *pdc=GetDC();
		for(i=0;i<50;i++){
			BuffDC.BitBlt(0,0,800,480,&MemDC,(k+i*4),0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,1);
		st_dialog=2;
	}
	else if(st_dialog==2)
	{
		int k=200;
		CDC *pdc=GetDC();
		for(i=0;i<50;i++){
			BuffDC.BitBlt(0,0,800,480,&MemDC,(k+i*4),0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,2);
		st_dialog=3;
	}
	else if(st_dialog==3)
	{
		int k=400;
		CDC *pdc=GetDC();
		for(i=0;i<50;i++){
			BuffDC.BitBlt(0,0,800,480,&MemDC,(k+i*4),0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,3);
		st_dialog=4;
	}
	else if(st_dialog==4)
	{
		int k=600;
		CDC *pdc=GetDC();
		for(i=0;i<50;i++){
			BuffDC.BitBlt(0,0,800,480,&MemDC,(k+i*4),0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,4);
		st_dialog=5;
	}//轩辕剑展示段结束
	else if(st_dialog==5)
	{
		vclose();
		PlayMidi("midi\\p1011.mid");
		Dialog(2,0,0,0,5);
		st_dialog=10;
	}
	//白莲教总部的片断
	else if(st_dialog==10){
		Dialog(1,0,0,0,66);
		st_dialog=11;
	}
	else if(st_dialog==11){
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_16);//白莲教总部
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,SRCCOPY);
		MapDC.BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_3);//沙子图片
		BuffDC.BitBlt(384,234,32,32,&MemDC,64,0,SRCAND);
		BuffDC.BitBlt(384,234,32,32,&MemDC,0,0,SRCINVERT);
		vopen();
		int a=384;
		int b=480;
		resetblt();
		MemDC.SelectObject(hbmp_4);//白莲教教徒
		for(i=0;i<10;i++){//移动
			blt(a,b,32,32,64,64,0,64,pdc);
			b-=8;
			GameSleep(50);
			blt(a,b,32,32,96,64,32,64,pdc);
			b-=8;
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,16,0,0,27);
		st_dialog=12;
	}
	else if(st_dialog==12){
		Dialog(4,0,11,0,28);
		st_dialog=13;
	}
	else if(st_dialog==13){
		Dialog(4,0,11,0,29);
		st_dialog=20;
	}//白莲教总部的片断结束
	//古墓总部的片断
	else if(st_dialog==20)
	{
		vclose();
		OpenMap("map\\map001.bmp","map\\map001.map","map\\map001.npc");//调入地图
		current.x=70;
		current.y=85;
		movest=0;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		resetblt();
		MemDC.SelectObject(hbmp_0);//ssitu
		CDC *pdc=GetDC();
		blt(386,216,32,32,64,64,0,64,pdc);
		resetblt();
		MemDC.SelectObject(hbmp_21);
		blt(386,250,32,32,0,32,0,0,pdc);
		vopen();
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(3,0,1,0,6);
		st_dialog=21;
	}
	else if(st_dialog==21){
		Dialog(3,0,0,0,7);
		st_dialog=22;
	}
	else if(st_dialog==22){
		PlayWave("snd//open.wav");
		MemDC.SelectObject(hbmp_21);
		CDC *pdc=GetDC();
		blt(354,250,96,32,32,32,32,0,pdc);
		GameSleep(500);
		blt(354,250,96,32,128,32,128,0,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		PlayWave("snd//prop.wav");
		GameSleep(500);
		Dialog(1,0,1,1,8);
		st_dialog=23;
	}
	else if(st_dialog==23){
		Dialog(4,0,1,0,9);
		st_dialog=24;
	}
	else if(st_dialog==24){
		Dialog(4,0,1,0,10);
		st_dialog=25;
	}
	else if(st_dialog==25){
		Dialog(4,0,1,0,11);
		st_dialog=26;
	}
	else if(st_dialog==26){
		PlayWave("snd//laugh.wav");
		Dialog(2,0,2,0,12);
		st_dialog=27;
	}
	else if(st_dialog==27){
		Dialog(2,0,0,0,13);
		st_dialog=28;
	}//OK//突然……（然后，FBI出现了）
	else if(st_dialog==28){
		MemDC.SelectObject(hbmp_2);//fbi_gun
		CDC *pdc=GetDC();

		resetblt();
		for(i=0;i<6;i++){//移动第一个FBI
			blt(300,(478-(i*32)),32,32,32,96,32,0,pdc);
			GameSleep(100);
			blt(300,(462-(i*32)),32,32,128,96,128,0,pdc);
			GameSleep(100);
		}
		blt(300,278,32,32,160,96,160,0,pdc);
		MemDC.SelectObject(hbmp_0);//ssitu
		oldlx=386;oldly=216;oldx=32;oldy=32;//涂抹老图像
		blt(370,200,64,32,64,0,0,0,pdc);
		MemDC.SelectObject(hbmp_2);//fbi_gun
		resetblt();
		for(i=0;i<5;i++){//移动第二个FBI
			blt(350,(478-(i*32)),32,32,32,96,32,0,pdc);
			GameSleep(100);
			blt(350,(462-(i*32)),32,32,128,96,128,0,pdc);
			GameSleep(100);
		}
		resetblt();
		for(i=0;i<8;i++){//移动第三个FBI
			blt(440,(478-(i*32)),32,32,32,96,32,0,pdc);
			GameSleep(100);
			blt(440,(462-(i*32)),32,32,128,96,128,0,pdc);
			GameSleep(100);
		}
		blt(440,198,32,32,96,128,96,32,pdc);
		MemDC.SelectObject(hbmp_1);//fbi_chief
		resetblt();
		for(i=0;i<14;i++){//移动FBI头目
			blt(384,(478-(i*16)),32,32,64,64,0,64,pdc);
			GameSleep(100);
			blt(384,(470-(i*16)),32,32,96,64,32,64,pdc);
			GameSleep(100);
		}
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,6,0,0,14);
		st_dialog=29;
	}//OK
	else if(st_dialog==29){
		Dialog(4,0,3,0,15);
		st_dialog=30;
	}
	else if(st_dialog==30){
		Dialog(4,6,0,0,16);
		st_dialog=31;
	}
	else if(st_dialog==31){
		Dialog(4,0,3,0,17);
		st_dialog=32;
	}
	else if(st_dialog==32){
		Dialog(4,6,0,0,18);
		st_dialog=33;
	}
	else if(st_dialog==33){
		MemDC.SelectObject(hbmp_3);//箱子
		oldlx=354;oldly=250;oldx=96;oldy=32;//涂抹老图像
		CDC *pdc=GetDC();
		blt(354,250,96,32,224,32,224,0,pdc);
		MemDC.SelectObject(hbmp_1);//fbi_chief
		resetblt();
		for(i=0;i<16;i++){//移动FBI头目
			blt(384,(282+(i*16)),32,32,64,0,0,0,pdc);
			GameSleep(100);
			blt(384,(290+(i*16)),32,32,96,0,32,0,pdc);
			GameSleep(100);
		}
		MemDC.SelectObject(hbmp_2);//fbi_gun
		resetblt();
		for(i=0;i<10;i++){//移动第三个FBI
			blt(440,(198+(i*32)),32,32,32,96,32,0,pdc);
			GameSleep(100);
			blt(440,(214+(i*32)),32,32,128,96,128,0,pdc);
			GameSleep(100);
		}
		resetblt();
		for(i=0;i<7;i++){//移动第一个FBI
			blt(300,(278+(i*32)),32,32,32,96,32,0,pdc);
			GameSleep(100);
			blt(300,(294+(i*32)),32,32,128,96,128,0,pdc);
			GameSleep(100);
		}
		resetblt();
		for(i=0;i<7;i++){//移动第二个FBI
			blt(350,(334+(i*32)),32,32,32,96,32,0,pdc);
			GameSleep(100);
			blt(350,(350+(i*32)),32,32,128,96,128,0,pdc);
			GameSleep(100);
		}
		MemDC.SelectObject(hbmp_0);//ssitu
		oldlx=370;oldly=200;oldx=64;oldy=32;//涂抹老图像
		blt(386,216,32,32,64,64,0,64,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(1000);
		Dialog(4,0,4,0,19);
		st_dialog=34;
	}
	else if(st_dialog==34){
		Dialog(4,0,5,0,20);
		st_dialog=35;
	}//OK//以上部分已经通过测试
	else if(st_dialog==35){
		vclose();
		current.x=97;
		current.y=25;
		movest=0;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_8);//大飞机图片
		BuffDC.BitBlt(214,120,374,239,&MemDC,374,0,SRCAND);
		BuffDC.BitBlt(214,120,374,239,&MemDC,0,0,SRCINVERT);
		vopen();
		MemDC.SelectObject(hbmp_1);//fbi_chief
		resetblt();
		CDC *pdc=GetDC();
		for(i=0;i<8;i++){//移动FBI头目
			blt(384,(478-(i*16)),32,32,64,64,0,64,pdc);
			GameSleep(100);
			blt(384,(470-(i*16)),32,32,96,64,32,64,pdc);
			GameSleep(100);
		}
		blt(0,0,0,0,0,0,0,0,pdc);//擦去小人
		MemDC.SelectObject(hbmp_2);//fbi_gun
		resetblt();
		for(int j=0;j<3;j++){
			for(i=0;i<8;i++){//移动第三个FBI
				blt(384,(478-(i*16)),32,32,32,160,32,64,pdc);
				GameSleep(100);
				blt(384,(470-(i*16)),32,32,128,160,128,64,pdc);
				GameSleep(100);
			}
		}
		blt(0,0,0,0,0,0,0,0,pdc);//擦去小人
		PlayWave("snd//motor.wav");
		MemDC.SelectObject(hbmp_8);//大飞机图片
		for(i=0;i<180;i++){
			blt(214,(120-i*2),374,239,374,0,0,0,pdc);
			GameSleep(10);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		vclose();
		Dialog(4,0,4,0,21);
		st_dialog=36;
	}//OK//以上部分已经通过测试
	else if(st_dialog==36){
		current.x=54;
		current.y=83;
		movest=0;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_9);//雷达屏幕
		BuffDC.BitBlt(240,20,260,300,&MemDC,260,0,SRCAND);
		BuffDC.BitBlt(240,20,260,300,&MemDC,0,0,SRCINVERT);
		MemDC.SelectObject(hbmp_0);//司徒
		BuffDC.BitBlt(360,300,32,32,&MemDC,64,128,SRCAND);
		BuffDC.BitBlt(360,300,32,32,&MemDC,0,128,SRCINVERT);
		vopen();
		Dialog(4,0,4,0,22);
		st_dialog=37;
	}//OK//以上部分已经通过测试
	else if(st_dialog==37){
		Dialog(4,0,4,0,23);
		st_dialog=38;
	}
	else if(st_dialog==38){
		CDC *pdc=GetDC();
		resetblt();
		MemDC.SelectObject(hbmp_9);//雷达屏幕
		PlayWave("snd//ding.wav");
		blt(380,100,4,4,400,200,52,12,pdc);//小绿点
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,4,0,24);
		st_dialog=39;
	}
	else if(st_dialog==39){
		Dialog(4,0,4,0,25);
		st_dialog=40;
	}
	else if(st_dialog==40){
		//导弹发射段
		vclose();
		current.x=13;
		current.y=43;
		movest=0;
		DrawMap();
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_10);//导弹发射井
		MapDC.BitBlt(272,176,256,128,&MemDC,0,0,SRCCOPY);
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		lopen();
		for(i=0;i<16;i++){
			resetblt();
			BuffDC.BitBlt((399-i*8),176,(i*16),128,&MemDC,(128-i*8),128,SRCCOPY);
			pdc->BitBlt(272,176,256,128,&BuffDC,272,176,SRCCOPY);
			GameSleep(50);
		}
		MapDC.BitBlt(272,176,256,128,&BuffDC,272,176,SRCCOPY);
		MemDC.SelectObject(hbmp_11);//导弹飞出段
		resetblt();
		for(i=0;i<220;i++){
			blt(367,(250-i*2),65,(i*2),65,0,0,0,pdc);
			GameSleep(4);
		}
		//屏幕显示段
		lclose();
		current.x=54;
		current.y=83;
		movest=0;
		DrawMap();
		MemDC.SelectObject(hbmp_9);//雷达屏幕
		MapDC.BitBlt(240,20,260,300,&MemDC,260,0,SRCAND);
		MapDC.BitBlt(240,20,260,300,&MemDC,0,0,SRCINVERT);
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		BuffDC.BitBlt(380,100,4,4,&MemDC,52,12,SRCCOPY);//小绿点
		MemDC.SelectObject(hbmp_0);//司徒
		BuffDC.BitBlt(360,300,32,32,&MemDC,64,128,SRCAND);
		BuffDC.BitBlt(360,300,32,32,&MemDC,0,128,SRCINVERT);
		vopen();
		resetblt();
		MemDC.SelectObject(hbmp_9);//雷达屏幕
		for(i=0;i<5;i++){
			PlayWave("snd//ding.wav");
			blt((350+i*6),100,4,4,400,200,164,212,pdc);//小红点
			GameSleep(500);
		}
		//飞机被击中段
		lclose();
		MemDC.SelectObject(hbmp_13);//岛上观海
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,BLACKNESS);
		BuffDC.BitBlt(90,0,619,480,&MemDC,0,0,SRCCOPY);
		MapDC.BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_14);//小飞机图片
		BuffDC.BitBlt(400,32,49,54,&MemDC,49,0,SRCAND);
		BuffDC.BitBlt(400,32,49,54,&MemDC,0,0,SRCINVERT);
		MemDC.SelectObject(hbmp_15);//小导弹图片
		BuffDC.BitBlt(640,46,70,10,&MemDC,0,0,SRCAND);
		BuffDC.BitBlt(640,46,70,10,&MemDC,0,10,SRCINVERT);
		lopen();
		PlayWave("snd\\bomb.wav");
		for(i=0;i<40;i++){
			BuffDC.BitBlt(200,0,600,200,&MapDC,200,0,SRCCOPY);
			MemDC.SelectObject(hbmp_14);//小飞机图片
			BuffDC.BitBlt((400-i*2),32,49,54,&MemDC,49,0,SRCAND);
			BuffDC.BitBlt((400-i*2),32,49,54,&MemDC,0,0,SRCINVERT);
			MemDC.SelectObject(hbmp_15);//小导弹图片
			BuffDC.BitBlt((640-i*7),46,70,10,&MemDC,0,0,SRCAND);
			BuffDC.BitBlt((640-i*7),46,70,10,&MemDC,0,10,SRCINVERT);
			pdc->BitBlt(200,0,600,200,&BuffDC,200,0,SRCCOPY);
			GameSleep(20);
		}
		//爆炸
		MemDC.SelectObject(hbmp_12);//爆炸图片
		for(i=0;i<10;i++){
			BuffDC.BitBlt(200,0,400,200,&MapDC,200,0,SRCCOPY);
			BuffDC.StretchBlt(300,32,108,100,&MemDC,(i*54),50,54,50,SRCAND);
			BuffDC.StretchBlt(300,32,108,100,&MemDC,(i*54),0,54,50,SRCINVERT);
			pdc->BitBlt(200,0,400,200,&BuffDC,200,0,SRCCOPY);
			GameSleep(100);
		}
		BuffDC.BitBlt(200,0,400,200,&MapDC,200,0,SRCCOPY);
		pdc->BitBlt(200,0,400,200,&BuffDC,200,0,SRCCOPY);
		GameSleep(1000);
		//屏幕显示段
		vclose();
		current.x=54;
		current.y=83;
		movest=0;
		DrawMap();
		MemDC.SelectObject(hbmp_9);//雷达屏幕
		MapDC.BitBlt(240,20,260,300,&MemDC,260,0,SRCAND);
		MapDC.BitBlt(240,20,260,300,&MemDC,0,0,SRCINVERT);
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		BuffDC.BitBlt(380,100,4,4,&MemDC,52,12,SRCCOPY);//小绿点
		MemDC.SelectObject(hbmp_0);//司徒
		BuffDC.BitBlt(360,300,32,32,&MemDC,64,128,SRCAND);
		BuffDC.BitBlt(360,300,32,32,&MemDC,0,128,SRCINVERT);
		vopen();
		MemDC.SelectObject(hbmp_9);//雷达屏幕
		blt(380,100,4,4,400,200,52,12,pdc);//小绿点
		PlayWave("snd//ding.wav");
		blt(380,100,4,4,400,200,164,212,pdc);//小红点
		GameSleep(500);
		blt(0,0,0,0,0,0,0,0,pdc);//擦去
		ReleaseDC(pdc);
		pdc = NULL;
		//
		Dialog(4,0,4,0,26);
		st_dialog=41;
	}//OK//以上部分已经通过测试
	else if(st_dialog==41){
		oldlx=360;
		oldly=300;
		oldx=32;
		oldy=32;
		int a=360;
		int b=300;
		MemDC.SelectObject(hbmp_0);//司徒
		CDC *pdc=GetDC();
		for(i=0;i<20;i++){//移动
			blt(a,b,32,32,64,160,0,160,pdc);
			a+=8;
			GameSleep(50);
			blt(a,b,32,32,96,160,32,160,pdc);
			a+=8;
			GameSleep(50);
		}
		for(i=0;i<10;i++){//移动
			blt(a,b,32,32,64,64,0,64,pdc);
			b+=8;
			GameSleep(50);
			blt(a,b,32,32,96,64,32,64,pdc);
			b+=8;
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		vclose();
		Dialog(2,0,0,0,30);
		st_dialog=50;
	}//OK//以上部分已经通过测试
	//古墓总部的片断结束
	else if(st_dialog==50){
		st_dialog=52;//先改状态再存盘
		Dialog(5,0,0,0,65);//参数5表示存盘
	}
	else if(st_dialog==52){
		if(type==2){
			st=2;//第一段
			st_sub1=0;//第一阶段
			st_dialog=53;//从下一句话开始
		}
		Dialog(2,0,0,0,31);
		st_dialog=54;
	}
	else if(st_dialog==54){
		PlayMidi("midi\\p1005.mid");
		OpenMap("map\\map001.bmp","map\\map001.map","map\\map001.npc");//调入地图
		current.x=70;
		current.y=82;
		movest=0;
		DrawMap();//绘制地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_5);//卧虫
		BuffDC.BitBlt(352,272,32,48,&MemDC,64,0,SRCAND);
		BuffDC.BitBlt(352,272,32,48,&MemDC,0,0,SRCINVERT);
		BuffDC.BitBlt(416,302,32,48,&ActorDC,96,48,SRCAND);//卡诺
		BuffDC.BitBlt(416,302,32,48,&ActorDC,0,48,SRCINVERT);
		vopen();
		Dialog(4,21,0,0,32);
		st_dialog=55;
	}//OK//以上部分已经通过测试
	else if(st_dialog==55){
		Dialog(4,0,26,0,33);
		st_dialog=56;
	}
	else if(st_dialog==56){
		Dialog(4,21,0,0,34);
		st_dialog=57;
	}
	else if(st_dialog==57){
		Dialog(4,0,26,0,35);
		st_dialog=58;
	}
	else if(st_dialog==58){
		Dialog(4,21,0,0,36);
		st_dialog=59;
	}
	else if(st_dialog==59){
		Dialog(4,0,26,0,37);
		st_dialog=60;
	}
	else if(st_dialog==60){
		Dialog(4,21,0,0,38);
		st_dialog=61;
	}
	else if(st_dialog==61){
		Dialog(4,0,26,0,39);
		st_dialog=62;
	}
	else if(st_dialog==62){
		Dialog(4,21,0,0,40);
		st_dialog=63;
	}
	else if(st_dialog==63){
		Dialog(4,0,26,0,41);
		st_dialog=64;
	}
	else if(st_dialog==64){
		Dialog(4,21,0,0,89);
		st_dialog=65;
	}
	else if(st_dialog==65){
		Dialog(4,0,26,0,90);
		st_dialog=66;
	}
	else if(st_dialog==66){
		Dialog(4,21,0,0,42);
		st_dialog=67;
	}
	else if(st_dialog==67){
		Dialog(4,21,0,0,43);
		st_sub1=1;
		st_dialog=0;
	}
}
/////////////////////////////////////////////////////////////////////////////
else if(st_sub1==1){
	if(st_dialog==0){
		int a=352;
		int b=272;
		resetblt();
		MemDC.SelectObject(hbmp_5);//卧虫
		CDC *pdc=GetDC();
		for(i=0;i<12;i++){//移动
			blt(a,b,32,48,64,0,0,0,pdc);
			b+=8;
			GameSleep(100);
			blt(a,b,32,48,96,0,32,0,pdc);
			b+=8;
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		vclose();
		Dialog(2,0,0,0,67);
		st_dialog=1;
	}
	else if(st_dialog==1){
		//PlayMidi("midi\\p1003.mid");
		current.x=70;
		current.y=85;
		oldcurrent.x=70;
		oldcurrent.y=85;
		azimuth= AZIMUTH_LEFT;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		vopen();
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2;//没有接续2的程序段，使程序在这里等待。
		st_progress=0;
	}
	//////////
	else if(st_dialog==3){
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2;//没有接续2的程序段，使程序在这里等待。
	}
	else if(st_dialog==4){
		Dialog(4,0,0,0,78);//营业员跟卡诺要QQ号
		st_dialog=5;
	}
	else if(st_dialog==5){//卡诺跟库管员说的话
		m_info_st=9;//对话态
		m_oldinfo_st=9;
		PlayWave("snd//prop.wav");
		GameSleep(500);
		Dialog(4,0,0,2,88);
		m_info_prop1=2;
		CDC *pdc=GetDC();
		RenewInfo(pdc);//在信息栏中显示物品小图
		ReleaseDC(pdc);
		pdc = NULL;
		st_dialog=6;
	}
	else if(st_dialog==6){//卡诺回营业员的话“不告诉你”
		Dialog(4,0,26,0,87);
		st_dialog=3;
	}
	//////////
	else if(st_dialog==10){//船离开后的对话
		lclose();
		m_info_st=9;//对话态
		m_oldinfo_st=9;
		PlayMidi("midi\\p1006.mid");
		current.x=35;
		current.y=164;
		movest=0;
		DrawMap();//绘制地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_5);//卧虫
		BuffDC.BitBlt(320,146,32,48,&MemDC,64,0,SRCAND);
		BuffDC.BitBlt(320,146,32,48,&MemDC,0,0,SRCINVERT);
		BuffDC.BitBlt(384,146,32,48,&ActorDC,96,0,SRCAND);//卡诺
		BuffDC.BitBlt(384,146,32,48,&ActorDC,0,0,SRCINVERT);
		MemDC.SelectObject(hbmp_18);//船
		BuffDC.BitBlt(200,304,333,120,&MemDC,0,0,SRCAND);
		BuffDC.BitBlt(200,304,333,120,&MemDC,333,0,SRCINVERT);
		MapDC.BitBlt(200,304,333,120,&BuffDC,200,304,SRCCOPY);
		lopen();
		resetblt();
		MemDC.SelectObject(hbmp_5);//上船
		int b=146;
		CDC *pdc=GetDC();
		for(i=0;i<10;i++){//移动
			blt(320,b,32,48,64,0,0,0,pdc);
			BuffDC.BitBlt(384,146,32,200,&MapDC,384,146,SRCCOPY);
			BuffDC.BitBlt(384,b,32,48,&ActorDC,128,0,SRCAND);//卡诺
			BuffDC.BitBlt(384,b,32,48,&ActorDC,32,0,SRCINVERT);
			pdc->BitBlt(384,146,32,200,&BuffDC,384,146,SRCCOPY);
			b+=8;
			GameSleep(100);
			blt(320,b,32,48,96,0,32,0,pdc);
			BuffDC.BitBlt(384,146,32,200,&MapDC,384,146,SRCCOPY);
			BuffDC.BitBlt(384,b,32,48,&ActorDC,160,0,SRCAND);//卡诺
			BuffDC.BitBlt(384,b,32,48,&ActorDC,64,0,SRCINVERT);
			pdc->BitBlt(384,146,32,200,&BuffDC,384,146,SRCCOPY);
			b+=8;
			GameSleep(100);
		}
		BuffDC.BitBlt(200,290,333,120,&MapDC,200,290,SRCCOPY);
		pdc->BitBlt(200,290,333,120,&MapDC,200,290,SRCCOPY);
		//船开出
		MemDC.SelectObject(hbmp_18);//船
		for(i=0;i<40;i++){//移动
			movest=13;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			BuffDC.BitBlt(200,304,333,120,&MemDC,0,120,SRCAND);
			BuffDC.BitBlt(200,304,333,120,&MemDC,333,120,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(10);
			movest=14;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			BuffDC.BitBlt(200,304,333,120,&MemDC,0,240,SRCAND);
			BuffDC.BitBlt(200,304,333,120,&MemDC,333,240,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(10);
			movest=15;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			BuffDC.BitBlt(200,304,333,120,&MemDC,0,120,SRCAND);
			BuffDC.BitBlt(200,304,333,120,&MemDC,333,120,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(10);
			movest=16;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			BuffDC.BitBlt(200,304,333,120,&MemDC,0,240,SRCAND);
			BuffDC.BitBlt(200,304,333,120,&MemDC,333,240,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(10);
			current.x+=1;
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,45);
		st_dialog=11;
	}//OK//以上部分已经调试通过
	else if(st_dialog==11){//海上
		vclose();
		MemDC.SelectObject(hbmp_19);//船上观海
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,BLACKNESS);
		BuffDC.BitBlt(200,0,400,480,&MemDC,0,0,SRCCOPY);
		MapDC.BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		vopen();
		Dialog(4,0,26,0,46);
		st_dialog=12;
	}
	else if(st_dialog==12){//海上
		Dialog(4,21,0,0,47);
		st_dialog=13;
	}
	else if(st_dialog==13){//海上
		Dialog(4,0,26,0,48);
		st_dialog=14;
	}
	else if(st_dialog==14){//海上
		Dialog(4,21,0,0,49);
		st_dialog=15;
	}
	else if(st_dialog==15){//海上
		Dialog(4,0,26,0,50);
		st_dialog=16;
	}
	else if(st_dialog==16){//海上
		Dialog(4,21,0,0,51);
		st_dialog=17;
	}
	else if(st_dialog==17){//海上
		Dialog(4,0,31,0,52);
		st_dialog=18;
	}
	else if(st_dialog==18){//海上
		Dialog(4,21,0,0,53);
		st_dialog=19;
	}
	else if(st_dialog==19){//海上
		Dialog(4,0,26,0,54);
		st_dialog=20;
	}
	else if(st_dialog==20){//海上
		vclose();
		vopen();
		MemDC.SelectObject(hbmp_20);//岛屿
		BuffDC.BitBlt(270,60,253,71,&MemDC,0,71,SRCAND);
		BuffDC.BitBlt(270,60,253,71,&MemDC,0,0,SRCINVERT);
		CDC *pdc=GetDC();
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,55);
		st_dialog=21;
	}
	else if(st_dialog==21){//海上
		Dialog(4,0,31,0,56);
		st_dialog=22;
	}
	else if(st_dialog==22){//海上
		Dialog(4,22,0,0,57);
		st_dialog=23;
	}
	else if(st_dialog==23){//海上
		Dialog(4,0,31,0,58);
		st_dialog=24;
	}
	else if(st_dialog==24){//海上
		Dialog(4,22,0,0,59);
		st_dialog=25;
	}
	else if(st_dialog==25){//海上
		Dialog(4,0,31,0,60);
		st_dialog=26;
	}
	else if(st_dialog==26){//海上
		Dialog(4,22,0,0,61);
		st_dialog=27;
	}
	else if(st_dialog==27){//海上
		Dialog(4,0,31,0,62);
		st_dialog=28;
	}
	else if(st_dialog==28){//海上
		Dialog(4,22,0,0,63);
		st_dialog=29;
	}
	else if(st_dialog==29){//海上
		Dialog(4,0,31,0,64);
		st_dialog=30;
	}
	else if(st_dialog==30){//海上
		MemDC.SelectObject(hbmp_20);//岛屿
		CDC *pdc=GetDC();
		for(i=0;i<30;i++){
			BuffDC.StretchBlt((270-i*4),60,(253+i*8),(71+i*2),&MemDC,0,71,253,71,SRCAND);
			BuffDC.StretchBlt((270-i*4),60,(253+i*8),(71+i*2),&MemDC,0,0,253,71,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(100);
		}
		PlayWave("snd\\bomb.wav");
		for(i=30;i<40;i++){
			BuffDC.StretchBlt((270-i*4),60,(253+i*8),(71+i*2),&MemDC,0,71,253,71,SRCAND);
			BuffDC.StretchBlt((270-i*4),60,(253+i*8),(71+i*2),&MemDC,0,0,253,71,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(100);
		}
		//摇晃
		quake();
		m_info_prop1=0;//消减物品
		RenewInfo(pdc);//在信息栏中显示物品小图
		ReleaseDC(pdc);
		pdc = NULL;
		lclose();
		PlayWave("snd//mdie.wav");
		GameSleep(1000);
		st_dialog=31;//存盘前先改参数
		Dialog(5,0,0,0,65);//参数5表示存盘
	}
//███████████████████████████████████████
	else if(st_dialog==31){
		st=3;//第二段
		st_sub1=0;//第一阶段
		st_dialog=0;
		Dialog(4,0,0,0,91);
		Para2Init();
	}
}
//DeleteObject(pdc);
}
//███████████████████████████████████████
//███这里是游戏情节的结束位置████████████████████████
//███████████████████████████████████████
void CMainFrame::Para1Accident(int type)
{
m_info_st=9;//对话态
m_oldinfo_st=9;
if(st_sub1==1){
	if(type==681){//路界1
		Dialog(4,0,26,0,70);
		st_dialog=3;
	}
	else if(type==763){//路界2
		Dialog(4,0,26,0,71);
		st_dialog=3;
	}
	else if(type==762){//路界3
		Dialog(4,0,26,0,72);
		st_dialog=3;
	}
	else if(type==684){//修路工
		Dialog(4,0,0,0,69);
		st_dialog=3;
	}
	else if(type==682){//河边老人
		Dialog(4,0,0,0,68);
		st_dialog=3;
	}
	else if(type==764){//导弹发射井
		Dialog(4,0,26,0,73);
		st_dialog=3;
	}
	else if(type==766){//收款员（已简化）
		Dialog(4,0,0,0,80);
		st_dialog=3;
	}
	else if(type==767){//仓库保管员（已简化）
		Dialog(4,0,0,0,82);
		st_dialog=3;
	}
	else if(type==641){//导弹控制台
		StopActTimer();
		MemDC.SelectObject(hbmp_17);//司徒工作室的广告
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,SRCCOPY);
		vopen();
		GameSleep(1000);
		Dialog(2,0,26,0,74);
		st_dialog=3;
	}
	//st_progress:3、已购得
	else if(type==643){//卧虫
		if(st_progress!=3){
			Dialog(4,21,0,0,76);
			st_dialog=3;
		}
		else{
			Dialog(4,21,0,0,77);
			st_dialog=10;//可以进入船离开的画面
		}
	}
	else if(type==765){//售货员（已简化）
		if(st_progress==0){
			st_progress=3;//买到货物
			Dialog(4,0,26,0,75);
			st_dialog=4;
		}
		else{
			Dialog(4,0,0,0,86);
			st_dialog=3;
		}
	}
}
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para1Timer()
{
}
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████