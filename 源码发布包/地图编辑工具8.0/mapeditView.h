// mapeditView.h : interface of the CMapeditView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAPEDITVIEW_H__D0A3BEC2_ED56_4F85_BB87_79A34750DB22__INCLUDED_)
#define AFX_MAPEDITVIEW_H__D0A3BEC2_ED56_4F85_BB87_79A34750DB22__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMapeditDoc;
class CMapeditView : public CView
{
protected: // create from serialization only
	CMapeditView();
	DECLARE_DYNCREATE(CMapeditView)

// Attributes
public:
	CMapeditDoc* GetDocument();

// Operations
public:
	BOOL run_once;//用于OnDraw中只执行一次的程序段
	int center_x;
	int center_y;//地图矩阵中当前操作位的坐标，值域0到199
	int selected;//地图单元集中当前单元的号码，值域0到800
	int tile_area;//当前显示的区号，值域1到4
	int cur_brush;//当前的画笔编号，
	//值域是（1，2，3，4，5，6，10）
	//该值*2-1即得到画刷的边长（单位：格）
	int buffer[19][19];//用于存放复制的图块（剪切板）
	//0表示边界
	HBITMAP m_pMapTile;//地图单元格位图
	HBITMAP m_pMiniMap;//背景草图
	CBitmap m_Buff;//主缓冲区（防止直接写屏幕造成闪烁）
	CFont m_ViewFont;
	//
	CDC TileDC;
	CDC CodeDC;
	CDC MiniDC;//背景草图专用
	CDC BuffDC;//主缓冲
	//
	BOOL m_bLButtonDown;
	BOOL m_bRButtonDown;
	//
	void displaymap(void);//显示用户区的所有信息
	void draw_map_ceil(CMapeditDoc* pDoc, int i, int x, int y,CDC *pbuffDC,int m,int n);//显示地图的一个方格
	void draw_evt_ceil(CMapeditDoc* pDoc, int i, int x, int y,CDC *pbuffDC,int m,int n);//显示一个事件方格
	void draw_table_lines(int cilnum,CDC *pbuffDC);//显示画面上所有的分界线
	void drawrect(CRect *prc,CDC *pbuffDC);//在主缓（*pbuffDC）上绘制一个方块（*prc）
	void draw_map_tiles(int area_num,CDC *pbuffDC);//在主缓上显示（area_num）区域的地图单元格位图
	void draw_edit_area(CMapeditDoc* pDoc, int lth,int hth,CDC *pbuffDC);
	void SetOneCell(int x, int y, int value);
	//在主缓（*pbuffDC）内绘制长lth，高hth的区域（可以调节地图显示区域的大小）
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMapeditView)
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	CString GetPathName(void);
	void GetThumbnail(CDC* pDC);
	void GetThumbnailSize(int* w, int* h);
	void SetCenterPos(int x, int y);
	void ExportPngFile(CString filename);
	//}}AFX_VIRTUAL

private:
	CScrollBar m_scoEditAreaH;
	CScrollBar m_scoEditAreaV;
	CScrollBar m_scoMapTilesH;
	BOOL m_bEditEventMode;

// Implementation
public:
	virtual ~CMapeditView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMapeditView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSet();
	afx_msg void OnErase();
	afx_msg void OnThumbnail();
	afx_msg void OnEditEventMode();
	afx_msg void OnUpdateEditEventMode(CCmdUI* pCmdUI);
	afx_msg void OnOpenEventFile();
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	afx_msg void OnPickup();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBrush1();
	afx_msg void OnUpdateBrush1(CCmdUI* pCmdUI);
	afx_msg void OnBrush2();
	afx_msg void OnUpdateBrush2(CCmdUI* pCmdUI);
	afx_msg void OnBrush3();
	afx_msg void OnUpdateBrush3(CCmdUI* pCmdUI);
	afx_msg void OnBrush4();
	afx_msg void OnUpdateBrush4(CCmdUI* pCmdUI);
	afx_msg void OnBrush5();
	afx_msg void OnUpdateBrush5(CCmdUI* pCmdUI);
	afx_msg void OnBrush6();
	afx_msg void OnUpdateBrush6(CCmdUI* pCmdUI);
	afx_msg void OnBrush7();
	afx_msg void OnUpdateBrush7(CCmdUI* pCmdUI);
	afx_msg void OnQuickDown();
	afx_msg void OnQuickLeft();
	afx_msg void OnQuickRight();
	afx_msg void OnQuickUp();
	afx_msg void OnStepRight();
	afx_msg void OnStepDown();
	afx_msg void OnStepLeft();
	afx_msg void OnStepUp();
	afx_msg void OnCopy();
	afx_msg void OnUpdateCopy(CCmdUI* pCmdUI);
	afx_msg void OnPaste();
	afx_msg void OnUpdatePaste(CCmdUI* pCmdUI);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in mapeditView.cpp
inline CMapeditDoc* CMapeditView::GetDocument()
   { return (CMapeditDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPEDITVIEW_H__D0A3BEC2_ED56_4F85_BB87_79A34750DB22__INCLUDED_)
