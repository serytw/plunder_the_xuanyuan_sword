// mapedit.h : main header file for the MAPEDIT application
//

#if !defined(AFX_MAPEDIT_H__69AB8DCB_29CE_451D_8F28_EE7DB43D57EE__INCLUDED_)
#define AFX_MAPEDIT_H__69AB8DCB_29CE_451D_8F28_EE7DB43D57EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMapeditApp:
// See mapedit.cpp for the implementation of this class
//
void DisableIME(HWND hWnd);

class CMapeditApp : public CWinApp
{
public:
	CMapeditApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMapeditApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	COleTemplateServer m_server;
		// Server object for document creation
	//{{AFX_MSG(CMapeditApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CMapeditApp theApp;
CString GetFileName(CString& strFullPath);
CString GetFilePath(CString& strFullPath);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPEDIT_H__69AB8DCB_29CE_451D_8F28_EE7DB43D57EE__INCLUDED_)
