// EditorView.cpp : implementation of the CEditorView class
//
#include "stdafx.h"
#include "Editor.h"
#include "EditorDoc.h"
#include "EditorView.h"

#include "CluList.h"
#include "ActList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditorView

IMPLEMENT_DYNCREATE(CEditorView, CView)

BEGIN_MESSAGE_MAP(CEditorView, CView)
	//{{AFX_MSG_MAP(CEditorView)
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditorView construction/destruction

CEditorView::CEditorView()
{
	// TODO: add construction code here
}

CEditorView::~CEditorView()
{
	delete m_pActList;
	delete m_pCluList;
}

BOOL CEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CEditorView drawing
void CEditorView::OnDraw(CDC* pDC)
{
	static BOOL bFirst = TRUE;
	if( bFirst )
	{
		CEditorDoc* pdoc = GetDocument();
		m_pActList = new CActList(this,pdoc,pdoc->m_pData);
		m_pCluList = new CCluList(this,pdoc,pdoc->m_pData);
		m_pCluList->Create();
		m_pActList->Create();
		//
		m_MemDC.CreateCompatibleDC( pDC );
		m_BufDC.CreateCompatibleDC( pDC );
		HBITMAP hbmp = (HBITMAP)LoadImage( NULL, "back.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );
		DeleteObject( m_MemDC.SelectObject(hbmp) );
		hbmp = (HBITMAP)LoadImage( NULL, "back.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );
		DeleteObject( m_BufDC.SelectObject(hbmp) );
		m_PointX = 0;
		m_PointY = 0;
		m_CX = 32;
		m_CY = 32;
		bFirst = FALSE;
	}
	m_BufDC.BitBlt( 0,0,640,480,&m_MemDC,0,0,WHITENESS );
	m_BufDC.BitBlt( 0,0,640,480,&m_MemDC,0,0,SRCCOPY );
	CPen GreenPen;
	GreenPen.CreatePen( PS_SOLID, 1, RGB(0,255,0) );
	DeleteObject( m_BufDC.SelectObject(GreenPen) );
	m_BufDC.MoveTo( m_PointX, m_PointY-32 );
	m_BufDC.LineTo( m_PointX, m_PointY+m_CY );
	m_BufDC.MoveTo( m_PointX-32, m_PointY );
	m_BufDC.LineTo( m_PointX+m_CX, m_PointY );
	m_BufDC.MoveTo( m_PointX+m_CX, m_PointY );
	m_BufDC.LineTo( m_PointX+m_CX, m_PointY+m_CY+1 );
	m_BufDC.MoveTo( m_PointX, m_PointY+m_CY );
	m_BufDC.LineTo( m_PointX+m_CX, m_PointY+m_CY );
	m_BufDC.SetBkMode( TRANSPARENT );
	m_BufDC.SetTextColor( RGB(0,0,255) );
	char str[255];
	itoa( m_PointX, str, 10 );
	m_BufDC.TextOut( 4,465,"X= " );
	m_BufDC.TextOut( 24,465,str );
	itoa( m_PointY, str, 10 );
	m_BufDC.TextOut( 104,465,"Y= " );
	m_BufDC.TextOut( 124,465,str );
	itoa( m_CX, str, 10 );
	m_BufDC.TextOut( 204,465,"CX= " );
	m_BufDC.TextOut( 234,465,str );
	itoa( m_CY, str, 10 );
	m_BufDC.TextOut( 304,465,"CY= " );
	m_BufDC.TextOut( 334,465,str );
	pDC->BitBlt( 0,0,640,480,&m_BufDC,0,0,SRCCOPY );
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CEditorView diagnostics

#ifdef _DEBUG
void CEditorView::AssertValid() const
{
	CView::AssertValid();
}

void CEditorView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CEditorDoc* CEditorView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CEditorDoc)));
	return (CEditorDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CEditorView message handlers

void CEditorView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	m_PointX = point.x;
	m_PointY = point.y;
	CDC* pDC = GetDC();
	OnDraw( pDC );
	ReleaseDC( pDC );
	CView::OnLButtonDown(nFlags, point);
}

void CEditorView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if( point.x>m_PointX && point.y>m_PointY )
	{
		m_CX = point.x - m_PointX;
		m_CY = point.y - m_PointY;
	}
	CDC* pDC = GetDC();
	OnDraw( pDC );
	ReleaseDC( pDC );
	CView::OnRButtonDown(nFlags, point);
}

BOOL CEditorView::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	m_pCluList->DestroyWindow();
	m_pActList->DestroyWindow();
	//
	return CView::DestroyWindow();
}

void CEditorView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	static BOOL bFirst = TRUE;
	if( bFirst )
	{
		bFirst = FALSE;
	}
	else
	{
		m_pCluList->UpdateList();
		m_pActList->UpdateList();
	}
}
