//DB.h			用于存储和操作系统数据堆的类
//
#if !defined(__GAMEDATA_H__)
#define __GAMEDATA_H__

class CGameData
{
public:
	BOOL Open(char* FileName);
	BOOL Save(char* FileName);
	//注意：参数都是1-based的，如果遇到0则不处理，返回FALSE。
	BOOL init();						//初始化系统数据堆。
	BOOL CPY(int sID,int dID);			//从sID拷贝到dID。
	BOOL SET(int num,int dID);			//将数字num赋给dID。
	BOOL ADD(int s1ID,int s2ID,int dID);
	BOOL SUB(int s1ID,int s2ID,int dID);
	BOOL MUL(int s1ID,int s2ID,int dID);
	BOOL DIV(int s1ID,int s2ID,int dID);
	BOOL MOD(int s1ID,int s2ID,int dID);
	BOOL IFB(int s1ID,int s2ID,int sID,int dID);
	BOOL IFS(int s1ID,int s2ID,int sID,int dID);
	BOOL IFE(int s1ID,int s2ID,int sID,int dID);
	BOOL AND(int s1ID,int s2ID,int dID);
	BOOL ORL(int s1ID,int s2ID,int dID);
	BOOL NOT(int sID,int dID);
	int	 Get(int ID);					//获取某个ID号所对应的ID值。
	int  GetCurSystem();		//获得当前运作系统的标号
	int  SetCurSystem(int cs);	//改写当前运作系统的标号
//
private:
	int nCurSystem;	//当前运作的系统：0、程序框架；1、对话场；2、战斗场。
	int data[256];	//系统数据堆
};

#endif