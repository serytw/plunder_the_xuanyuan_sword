//Enemy.h

#if !defined(__ENEMY_H__)
#define __ENEMY_H__

//
#define CX1 32
#define CY1 32		//第一种敌人的长宽
#define CX2 22
#define CY2 32		//第二种敌人的长宽
#define CX3 14
#define CY3 22		//第三种敌人的长宽
//
#define X1A 0
#define Y1A 0
#define X1B 32
#define Y1B 0
#define X1C 64
#define Y1C 0
#define X1D 96
#define Y1D 0
//
#define X2A 0
#define Y2A 32
#define X2B 22
#define Y2B 32
#define X2C 44
#define Y2C 32
#define X2D 66
#define Y2D 32
//
#define X3A 0
#define Y3A 64
#define X3B 14
#define Y3B 64
#define X3C 28
#define Y3C 64
#define X3D 42
#define Y3D 64
//
#define DELAY1 16		//出现到射击的一段延时
#define DELAY2 6		//射击后到消失的一段延时
#define DELAY3 20		//死亡过程
//
#define TYPE1COST 5		//定义一类敌人分值
#define TYPE2COST 10	//定义二类敌人分值
#define TYPE3COST 20	//定义三类敌人分值
#define ENEMYPOWER 10	//定义敌人的威力
//
#define INTERVAL 30		//定义敌人的出现间隙大约2秒
//
class CEnemy
{
public:
	CEnemy();
	~CEnemy();
	BOOL Init(HDC hBmpDC,CGameData* pD,CMidi* pM);
	BOOL Set(int mode,int tx,int ty,int time);
	void Paint(HWND hwnd,HDC hdc);
	BOOL OnShot(int tx,int ty);
	BOOL IsLiving();
//
private:
	BOOL Fire();		//开火函数
	HDC hMemDC;			//用于存放操作位图的设备
	int x,y;			//显示定位
	int npx,npy,npcx,npcy;	//位图的当前显示位置和当前显示区域
	int nState;			//生命状态
	//0不活动；1等待出现；2出现过程；3等待一；4射击；5等待二；6消失过程；7死亡过程
	int nType;			//敌人类型和出现方式
	//1一号敌人向上出现；2一号敌人向左出现；3一号敌人向右出现
	//4二号敌人向上出现；5二号敌人向左出现；6二号敌人向右出现
	//7三号敌人向上出现；8三号敌人向左出现；9三号敌人向右出现
	//0未出现；1出现1/4；2出现1/2；3出现3/4；4完全出现
	int nStartDelay;	//起始延时
	int nDelay;			//过程中延时
	CMidi* m_pMidi;		//播放声音的类
	CGameData* m_pDB;	//系统数据堆
};

#endif