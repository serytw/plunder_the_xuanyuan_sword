//db.cpp

#include "stdafx.h"


//
BOOL CGameData::Open(char* FileName)
{
	HANDLE hFile;
	hFile = CreateFile(FileName,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
	DWORD dwActualSize = 0;
	BOOL bReadSt;
	//读出当前系统标识
	bReadSt = ReadFile(hFile,&nCurSystem,sizeof(int),&dwActualSize,NULL);
	if( !bReadSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//读出数据堆
	bReadSt = ReadFile(hFile,data,(sizeof(int)*256),&dwActualSize,NULL);
	if( !bReadSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//
	CloseHandle(hFile);
	return TRUE;
}
//
BOOL CGameData::Save(char* FileName)
{
	//写入文件
	HANDLE hFile;
	hFile = CreateFile(FileName,GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ,0,CREATE_ALWAYS,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
	DWORD dwWritten = 0;
	BOOL bWriteSt;
	//写入当前系统标识
	bWriteSt = WriteFile(hFile,&nCurSystem,sizeof(int),&dwWritten,NULL);
	if ( !bWriteSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//写入数据堆
	bWriteSt = WriteFile(hFile,data,(sizeof(int)*256),&dwWritten,NULL);
	if ( !bWriteSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//
	CloseHandle(hFile);
	return TRUE;
}
//
BOOL CGameData::init()
{
	nCurSystem = 0;
	memset(data,0x0,sizeof(int)*256);
	data[0] = 1;	//游戏进程ID
	data[1] = 300;	//生命
	data[2] = 0;	//金钱
	data[3] = 60;	//手枪子弹数目
	data[4] = 0;	//机枪子弹数目
	data[5] = 0;	//炮弹数目
	data[6] = 0;	//当前武器状况
	data[7] = 0;	//对话选择
	return TRUE;
};
//
BOOL CGameData::CPY(int sID,int dID)
{
	if( sID<1 || sID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		data[dID-1] = data[sID-1];
		return TRUE;
	}
};
//
BOOL CGameData::SET(int num,int dID)
{
	if( dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		data[dID-1] = num;
		return TRUE;
	}
};
//
BOOL CGameData::ADD(int s1ID,int s2ID,int dID)
{
	if( s1ID<1 || s1ID>256 || s2ID<1 || s2ID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		data[dID-1] = ( data[s1ID-1] + data[s2ID-1] );
		return TRUE;
	}
};
//
BOOL CGameData::SUB(int s1ID,int s2ID,int dID)
{
	if( s1ID<1 || s1ID>256 || s2ID<1 || s2ID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		data[dID-1] = ( data[s1ID-1] - data[s2ID-1] );
		return TRUE;
	}
};
//
BOOL CGameData::MUL(int s1ID,int s2ID,int dID)
{
	if( s1ID<1 || s1ID>256 || s2ID<1 || s2ID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		data[dID-1] = ( data[s1ID-1] * data[s2ID-1] );
		return TRUE;
	}
};
//
BOOL CGameData::DIV(int s1ID,int s2ID,int dID)
{
	if( s1ID<1 || s1ID>256 || s2ID<1 || s2ID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		data[dID-1] = ( data[s1ID-1] / data[s2ID-1] );
		return TRUE;
	}
};
//
BOOL CGameData::MOD(int s1ID,int s2ID,int dID)
{
	if( s1ID<1 || s1ID>256 || s2ID<1 || s2ID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		data[dID-1] = ( data[s1ID-1] % data[s2ID-1] );
		return TRUE;
	}
};
//
BOOL CGameData::IFB(int s1ID,int s2ID,int sID,int dID)
{
	if( s1ID<1 || s1ID>256 || s2ID<1 || s2ID>256 || sID<1 || sID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		if( data[s1ID-1] > data[s2ID-1] )
		{
			data[dID-1] = data[sID-1] ;
		}
		return TRUE;
	}
};
//
BOOL CGameData::IFS(int s1ID,int s2ID,int sID,int dID)
{
	if( s1ID<1 || s1ID>256 || s2ID<1 || s2ID>256 || sID<1 || sID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		if( data[s1ID-1] < data[s2ID-1] )
		{
			data[dID-1] = data[sID-1] ;
		}
		return TRUE;
	}
};
//
BOOL CGameData::IFE(int s1ID,int s2ID,int sID,int dID)
{
	if( s1ID<1 || s1ID>256 || s2ID<1 || s2ID>256 || sID<1 || sID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		if( data[s1ID-1] == data[s2ID-1] )
		{
			data[dID-1] = data[sID-1] ;
		}
		return TRUE;
	}
};
//
BOOL CGameData::AND(int s1ID,int s2ID,int dID)
{
	if( s1ID<1 || s1ID>256 || s2ID<1 || s2ID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		if( data[s1ID-1]!=0 && data[s2ID-1]!=0 )
		{
			data[dID-1] = 1;
		}
		else
		{
			data[dID-1] = 0;
		}
		return TRUE;
	}
};
//
BOOL CGameData::ORL(int s1ID,int s2ID,int dID)
{
	if( s1ID<1 || s1ID>256 || s2ID<1 || s2ID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		if( data[s1ID-1]!=0 || data[s2ID-1]!=0 )
		{
			data[dID-1] = 1;
		}
		else
		{
			data[dID-1] = 0;
		}
		return TRUE;
	}
};
//
BOOL CGameData::NOT(int sID,int dID)
{
	if( sID<1 || sID>256 || dID<1 || dID>256 )
	{
		return FALSE;
	}
	else
	{
		if( data[sID-1]==0 )
		{
			data[dID-1] = 1;
		}
		else
		{
			data[dID-1] = 0;
		}
		return TRUE;
	}
};
//
int	 CGameData::Get(int ID)
{
	if( ID<1 || ID>256 )
	{
		return( -32768 );
	}
	else
	{
		return( data[ID-1] );
	}
};
//
int  CGameData::GetCurSystem()
{
	return( nCurSystem );
}
//
int  CGameData::SetCurSystem(int cs)
{
	int n = nCurSystem;
	nCurSystem = cs;
	return( n );
}
//the end.